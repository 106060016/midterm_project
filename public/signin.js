function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    //var btnForget = document.getElementById('btnForget');
    var fname =document.getElementById("inputfName");
    var lname =document.getElementById("inputlName");
    var birthday =document.getElementById("inputBirthday");
    var gender =document.getElementById("inputGender");

    btnLogin.addEventListener('click', function () {
        var email = txtEmail.value;
        var password =txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email,password)
        .then(function(result){
            window.location ="homepage.html";
        })
        .catch(function(error) {
            txtEmail.value ="";
            txtPassword.value ="";
            create_alert("error","You Cannot Sign in");
        });
    });

    btnGoogle.addEventListener('click', function () {

        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider)
            .then(function(result) {
                var token =result.credential.accessToken;
                var user = result.user;
                window.location ="homepage.html";
            })
            .catch(function(error) {
                var errorCode =error.code;
                var errorMessage =error.message;
                var email =error.email;
                var credential =error.credential;
                create_alert("error",'You Cannot Sign in with Google');
            });
        /*firebase.auth().signInWithRedirect(provider);
        firebase.auth().getRedirectResult().then(function(result) {
            if (result.credential) {
            // This gives you a Google Access Token. You can use it to access the Google API.
                var token = result.credential.accessToken;
            // ...
            }
            // The signed-in user info.
            var user = result.user;
            }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            create_alert("error",'You Cannot Login');
        });*/
    });

    btnSignUp.addEventListener('click', function () {  
        var email = txtEmail.value;
        var password =txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email,password)
        .then (function(result){
            if (fname.value != ""&&lname.value != ""&&birthday.value != ""&&gender.value != "") {
                txtEmail.value ="";
                txtPassword.value ="";
                create_alert("success",'Sign up!!');
                firebase.database().ref('user_list').push({
                    email : email,
                    fname : fname.value,
                    lname : lname.value,
                    birthday : birthday.value,
                    gender : gender.value
                })
            }
            else{
                create_alert("error", "Please complete all the input");
            }
        })
        .catch(function(error) {
            txtEmail.value ="";
            txtPassword.value ="";
            create_alert("error",'You Cannot Sign up');
        });      
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};