function init(){
    //console.log(user.message);
    var useremail='';
    console.log(user_email.innerHTML);
    firebase.auth().onAuthStateChanged(function (user) {
        useremail=user.email;
        firebase.database().ref('user_list').once('value').then(
        function(snapshot){
            var obj=snapshot.val();
            for(var key in obj){
                if(obj[key].email==useremail){
                    user_email.innerHTML+= obj[key].email;
                    user_fname.innerHTML+= obj[key].fname;
                    user_lname.innerHTML+= obj[key].lname;
                    user_birthday.innerHTML+= obj[key].birthday;
                    user_gender.innerHTML+= obj[key].gender;
                }
            }
        }
    )
    });
    
    btnLogout.addEventListener('click', function () {
        firebase.auth().signOut().then(function() {
            alert("Successfully Logout");
            window.location ="signin.html";
        }).catch(function(error) {
            alert("Error!!");
        });
    });
    btnChange.addEventListener('click', function () {
        firebase.auth().signOut().then(function() {
            alert("Successfully Logout");
            window.location ="signin.html";
        }).catch(function(error) {
            alert("Error!!");
        });
    });
    AForum.addEventListener('click', function () {
            alert("Lets Go to A Forum!");
            window.location ="index.html";
    });
    BForum.addEventListener('click', function () {
        alert("Lets Go to B Forum!");
        window.location ="index1.html";
    });
    CForum.addEventListener('click', function () {
        alert("Lets Go to C Forum!");
        window.location ="index2.html";
    });
    function create_alert(type, message) {
        var alertarea = document.getElementById('custom-alert');
        if (type == "success") {
            str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            alertarea.innerHTML = str_html;
        } else if (type == "error") {
            str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            alertarea.innerHTML = str_html;
        }
    }
}
window.onload = function(){
    init();
};