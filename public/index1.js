function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='btnMypage'>"+user.email+" 's Forum</span><span class='dropdown-item' id='btnLogout'>Logout</span>";
            
            btnLogout.addEventListener('click', function () {
                firebase.auth().signOut().then(function() {
                    alert("Successfully Logout");
                }).catch(function(error) {
                    alert("Error!!");
                });
            });
            btnMypage.addEventListener('click',function(){
                window.location ="homepage.html";
            })
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function () {
        var dt = new Date();
        if (post_txt.value != "") {
            console.log("read");
            firebase.database().ref('com_list').push({
                email : user_email,
                comment : post_txt.value,
                cnt : '1',
                time : dt.getFullYear()+"."+dt.getMonth()+"."+dt.getDay()+"."+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds()
            })
            post_txt.value="";
        }
    });

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Resent updates</h6><div class='media text-muted pt-3' style='display:block'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.on('child_added',snapshot=>{
        var post_list = document.getElementById("post_list");
        var maindiv =document.createElement("div");
        maindiv.innerHTML =str_before_username + snapshot.val().email + "</strong></p><p id= '"+snapshot.val().comment+"'>" + snapshot.val().comment+"</p><p>" + snapshot.val().time + str_after_content;
        if(snapshot.val().cnt==1){
            post_list.appendChild(maindiv);
            console.log(snapshot.val().comment);
            setTimeout( function(){maindiv.addEventListener('click',function(){
                console.log('clicked.');
                var topic=snapshot.val().comment;
                post_list.innerHTML="<div style='background-color:lavender; width: 80%; height: 200px;'>"+snapshot.val().comment+"</div>";
                var msg =document.createElement("input");
                msg.setAttribute("type", "input");
                msg.setAttribute("placeholder","boy/girl");
                msg.setAttribute("class","form-control");
                var msgsubmit =document.createElement("button");
                msgsubmit.setAttribute("type", "button");
                msgsubmit.className = "btn btn-lg btn-dark btn-block";
                msgsubmit.innerHTML ="Complete";
                
                //post_list.innerHTML+=`<input type="text" id="msg" class="form-control" placeholder="boy/girl" required><button class="btn btn-lg btn-dark btn-block" id="msgsubmit">Complete</button>`;
                //var msg =document.getElementById("msg");
                //var msgbtn =document.getElementById("msgsubmit");
                msgsubmit.addEventListener('click', function () {
                    console.log("msg1");
                    if (msg.value != "") {
                        firebase.database().ref('msg_list').push({
                            topic :topic,
                            msg : msg.value
                        })
                    }
                    msg.value="";
                });
                post_list.appendChild(msg);
                post_list.appendChild(msgsubmit);
                firebase.database().ref('msg_list').once('value').then(
                    function(snapshot){
                        console.log(topic);
                        var obj=snapshot.val();
                        var AAA = document.createElement("DIV");
                        for(var key in obj){
                            if(obj[key].topic==topic){
                                AAA.innerHTML += obj[key].msg + "<br>";
                            }
                        }
                        post_list.appendChild(AAA);
                    }
                );
            });}, 1000);

        }
        console.log("end");
        //post_list.innerHTML = post_list.innerHTML + new_post;
    })
    

    // const functions = require('firebase-functions');
    // exports.addedQQdata = functions.database.ref('/com_list/{pushId}')
    // .onCreate((snapshot, context) => {
    //     const original = snapshot.val().comment;
    //     //console.log('Uppercasing', context.params.pushId, original);
    //     const addedQQ = original +"QQ";
    //     return snapshot.ref.child('addedQQdata').set(addedQQ);
    // });
}

window.onload = function () {
    init();
};