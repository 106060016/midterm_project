# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : 106060016_midterm
* Key functions (add/delete)
    1. 發文
    2. 登入登出
    3. 回帖
    4. 用GOOLE登入
    5. 註冊
    
* Other functions (add/delete)
    1. 可以顯示發文時間
    2. 在主頁有顯示個人資料
    3. 一開始signin的動畫
    4. 進入頁面或是登入成功/失敗的提示

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：[https://midterm-f514e.firebaseapp.com]

# Components Description : 
1. Signin/Signup : 若第一次用，要先創建帳號並且要填滿下面四個基本資料才可以按下new Account創建成功，好了之後再輸入一次並且按下signin就可以登錄成功，跳到homepage.html。
2. Host on firebase : 我有使用firebase deploy
3. Database read/write : 我用了database的com_list存發文，msg_list存回帖，user_list存個人基本資料(把打的都push進去)。
4. RWD : 有用bootstrap使當夜面大小變換時不會變形
5. Userpage : 登入後會進入的頁面，連接我的三個論壇
6. postlistpage: 進入論壇會顯示之前的每一則貼文，就是postlist
7. postpage : 用div給每一則的貼文，當點了貼文就會只顯現出那一則貼文並有一個input跟button
8. leavecomment : 根據7.的input跟button就可以在postpage的下面回帖內容，並且當重新整理頁面後會顯現出來
9. Signin with google: 在登錄頁面我有使用sign in with google 就跟lab06的其實一樣
10. use css animation : 在signin的時候會有ㄧ些動畫，背景會無限的變色，一開始我的unput框會左右移動

# Other Functions Description(1~10%) : 
1. 顯示時間: 有在每一個key的下面把時間存進database，就可以顯示美一則貼文的時間
2. 顯示個人資料:在signin 的時候創新帳號時輸入的四個基本資料存進msg_list裡，在主頁裡顯示出來。
3. 進入頁面或是登入成功/失敗的提示:在signin或是創新帳號時都會有顯示成功或失敗，進入不同的msgboard的時候也會有題是在告訴進入不同forum
...

## Security Report (Optional)
在database.rules.json中有寫".read":"auth!=null" ".write":"auth!=null"，就是當沒有登錄認證，就不能看到跟撰寫貼文。